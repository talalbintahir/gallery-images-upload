package com.talalbintahir.galleryimagesupload;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesActivity;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class ScreenTest {


    @Rule
    public ActivityTestRule<GalleryOfImagesActivity> mActivityRule =
            new ActivityTestRule<>(GalleryOfImagesActivity.class);

    /**
     *  This test clicks the Floating button to open the picture gallery
     * @throws Exception
     */
    @Test
    public void clickAddNewImageButton_opensUpTheGallery() throws Exception {
        onView(withId(R.id.btn_add_new_image))
                .perform(click());
    }

    /**
     * This test open the image detail screen
     * @throws Exception
     */
    @Test
    public void clickTheImage_opensUpImageDetailScreen() throws Exception {
        onView(withId(R.id.rv_images))
                .perform(click());
    }
    /**
     * This test open the image and then click the edit floating button
     * to crop and rotate the image and save it by clicking the done button
     * @throws Exception
     */
    @Test
    public void clickEditImageButton_opensUpCropAndRotateImageScreen_clicksTheDoneButton() throws Exception {
        onView(withId(R.id.rv_images))
                .perform(click());
        onView(withId(R.id.btn_edit_image))
                .perform(click());
        onView(withText(R.string.done))
                .perform(click());
    }

    /**
     * This test open the image and swipe left to see other images
     * @throws Exception
     */
    @Test
    public void openTheImage_SwipeLeft() throws Exception {
        onView(withId(R.id.rv_images))
                .perform(click());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
    }

    /**
     * This test open the image, swipe left, click the edit floating button and
     * then finally click the done button to save the image.
     * @throws Exception
     */
    @Test
    public void openTheImage_SwipeLeft_clickEditImageButton_opensUpCropAndRotateImageScreen_clicksTheDoneButton() throws Exception {
        onView(withId(R.id.rv_images))
                .perform(click());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
        onView(withId(R.id.viewpager_image_detail))
                .perform(swipeLeft());
        onView(withId(R.id.btn_edit_image))
                .perform(click());
        onView(withText(R.string.done))
                .perform(click());
    }
}
