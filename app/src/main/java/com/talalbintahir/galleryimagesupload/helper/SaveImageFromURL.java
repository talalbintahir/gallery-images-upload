package com.talalbintahir.galleryimagesupload.helper;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.activity.ImageCropAndRotatePresenter;
import com.talalbintahir.galleryimagesupload.activity.ImageCropAndRotatePresenterImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class save online image and then send it to get edit on image crop and rotate activity
 */
public class SaveImageFromURL extends AsyncTask<String, Void, Bitmap> {

  private final Activity mActivity;
  public SaveImageFromURL(Activity activity) {
    this.mActivity = activity;
  }
  @Override
  protected Bitmap doInBackground(String... url) {
    String imageURL = url[0];
    Bitmap bitmap = null;
    try {
      InputStream srt = new java.net.URL(imageURL).openStream();
      bitmap = BitmapFactory.decodeStream(srt);
    } catch (Exception e){
      e.printStackTrace();
    }
    return bitmap;
  }

  @Override
  protected void onPostExecute(Bitmap bitmap) {
    super.onPostExecute(bitmap);
    saveImageToInternalStorage(bitmap);
  }

  private void saveImageToInternalStorage(Bitmap bitmap){
    // Initialize ContextWrapper

    ContextWrapper wrapper = new ContextWrapper(mActivity);

    // Initializing a new file
    // The bellow line return a directory in internal storage
    File file = wrapper.getDir("Images",MODE_PRIVATE);

    // Create a file to save the image
    file = new File(file, "UniqueFileName"+".jpg");

    try{
      // Initialize a new OutputStream
      OutputStream stream;

      // If the output file exists, it can be replaced or appended to it
      stream = new FileOutputStream(file);

      // Compress the bitmap
      bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

      // Flushes the stream
      stream.flush();

      // Closes the stream
      stream.close();

    }catch (IOException e) // Catch the exception
    {
      e.printStackTrace();
    }

    // Parse the gallery image url to uri
    Uri savedImageURI = Uri.parse(mActivity.getResources().getString(R.string.content_provider)+Uri.parse(file.getAbsolutePath()));
    // Presenter instance of the imageCropAndRotateActivity
    ImageCropAndRotatePresenter imageCropAndRotatePresenter = new ImageCropAndRotatePresenterImpl();
    imageCropAndRotatePresenter.editImageFromURL(savedImageURI,mActivity);// this method is called to start CroppingImage activity
  }

}