package com.talalbintahir.galleryimagesupload.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;
import java.util.List;

/**
 * ImageAdapter class for the RecyclerView in Gallery of image activity
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

  private final List<ImagesModelImpl> images;
  private final Context mContext;

  public class MyViewHolder extends RecyclerView.ViewHolder {
    public final ImageView thumbnail;

    public MyViewHolder(View view) {
      super(view);
      thumbnail = view.findViewById(R.id.thumbnail); // thumbnail image which is seen on the first screen
    }
  }

  public ImageAdapter(Context context, List<ImagesModelImpl> images) {
    mContext = context;
    this.images = images;
  }

  @NonNull
  @Override
  public ImageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.image_thumbnail, parent, false);

    return new ImageAdapter.MyViewHolder(itemView);
  }

  /**
   *  To bind the view holder
   */
  @Override
  public void onBindViewHolder(@NonNull ImageAdapter.MyViewHolder holder, int position) {
    ImagesModelImpl image = images.get(position);
    RequestOptions requestOptions = new RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true);
    Glide.with(mContext) // Glide is an image loading framework
        .load(image.getImagePath()) // loads images
        .apply(requestOptions)
        .transition(DrawableTransitionOptions.withCrossFade()) // cross fade transitions
        .into(holder.thumbnail); // show as thumbnail
  }

  @Override
  public int getItemCount() {
    return images.size();
  }

  public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
  }

  public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private final GestureDetector gestureDetector;
    private final ImageAdapter.ClickListener clickListener;

    public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ImageAdapter.ClickListener clickListener) {
      this.clickListener = clickListener;
      gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
          return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
          View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
          if (child != null && clickListener != null) {
            clickListener.onLongClick(child, recyclerView.getChildPosition(child));
          }
        }
      });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

      View child = rv.findChildViewUnder(e.getX(), e.getY());
      if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
        clickListener.onClick(child, rv.getChildPosition(child));
      }
      return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
  }
}