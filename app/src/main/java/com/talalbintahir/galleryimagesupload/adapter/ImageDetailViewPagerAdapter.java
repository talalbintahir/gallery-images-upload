package com.talalbintahir.galleryimagesupload.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;
import java.util.ArrayList;

public class ImageDetailViewPagerAdapter extends PagerAdapter {

  private LayoutInflater layoutInflater;
  private Activity mActivity;
  private ArrayList<ImagesModelImpl> images;

  public ImageDetailViewPagerAdapter(Activity mActivity, ArrayList<ImagesModelImpl> images) {
    this.mActivity = mActivity;
    this.images = images;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {

    layoutInflater = (LayoutInflater) mActivity
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);

    ImageView imageViewPreview = view.findViewById(R.id.iv_image_fullscreen_preview);

    ImagesModelImpl image = images.get(position);

    RequestOptions requestOptions = new RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true);

    Glide.with(mActivity)
        .load(image.getImagePath())
        .apply(requestOptions)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(imageViewPreview);

    container.addView(view);

    return view;
  }

  @Override
  public int getCount() {
    return images.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object obj) {
    return view == ((View) obj);
  }


  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

}
