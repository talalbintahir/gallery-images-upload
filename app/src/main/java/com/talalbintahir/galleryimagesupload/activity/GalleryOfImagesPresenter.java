package com.talalbintahir.galleryimagesupload.activity;

import android.content.Intent;

public interface GalleryOfImagesPresenter {

  void getOnlineImages();

  void showProgressDialog();

  void hideProgressDialog();

  void permissionDenied();

  void openImageDetailFragment(int position);

  void openImagePicker();

  void addNewImage(int requestCode, int resultCode, Intent resultData);
}
