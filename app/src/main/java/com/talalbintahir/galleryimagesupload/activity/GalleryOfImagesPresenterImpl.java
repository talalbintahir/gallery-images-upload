package com.talalbintahir.galleryimagesupload.activity;

import android.content.Context;
import android.content.Intent;
import com.esafirm.imagepicker.features.ImagePicker;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.model.ImagesModel;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;

/**
 * Class to implement method from presenter class of Gallery of Images activity which connects its View and Model.
 */
public class GalleryOfImagesPresenterImpl implements GalleryOfImagesPresenter {

  private final GalleryOfImagesActivity galleryOfImagesActivity;
  private ImagesModel imagesModel;
  private final Context mContext;

  public GalleryOfImagesPresenterImpl(GalleryOfImagesActivity galleryOfImagesActivity, Context context) {
    this.galleryOfImagesActivity = galleryOfImagesActivity;
    mContext = context;
  }

  /**
   * This method gets the images from online
   */
  @Override
  public void getOnlineImages() {
    imagesModel = new ImagesModelImpl(); // instance of model
    imagesModel.fetchImages(this, mContext); // to fetch the images
    galleryOfImagesActivity.setRecycleViewAdapter(imagesModel.getAdapter()); // sets the recyclerView in activity
  }

  /**
   * Call the view method to show progress dialog
   */
  @Override
  public void showProgressDialog() {
    galleryOfImagesActivity.showProgressDialog();
  }

  /**
   * Call the view method to hide progress dialog
   */
  @Override
  public void hideProgressDialog() {
    galleryOfImagesActivity.hideProgressDialog();
  }

  /**
   * THis method checks if the permission were denied by the user of not
   * to show the final dialog of permission
   */
  @Override
  public void permissionDenied() {
    if(galleryOfImagesActivity.shouldShowDialog()) galleryOfImagesActivity.showPermissionDialog();
    else galleryOfImagesActivity.showUnlockPermissionsDialog();
  }

  /**
   * This method open the fragment of the image at the given position
   * @param position is the position of the image
   */
  @Override
  public void openImageDetailFragment(int position) {
    galleryOfImagesActivity.startImageDetailFragment(position);
  }

  /**
   * The method open the image picker
   */
  @Override
  public void openImagePicker() {
    if (!galleryOfImagesActivity.checkPermission()) {
      // check if the user has given access or not
      galleryOfImagesActivity.showPermissionDialog();
      return;
    }
    // add the image
    galleryOfImagesActivity.addNewImage();
  }

  /**
   * This method gets the result of the image picker and then add it in the adapter
  */
  @Override
  public void addNewImage(int requestCode, int resultCode, Intent resultData) {
    if (ImagePicker.shouldHandle(requestCode, resultCode, resultData)) {
      if (ImagePicker.getImages(resultData).size() > 1) {
        // checks if the picked images were more than one or not
        for (int i = 0; i < ImagePicker.getImages(resultData).size(); i++) {
          imagesModel.addNewImageInAdapter(
              ImagePicker.getImages(resultData).get(i).getPath(),
              galleryOfImagesActivity.getResources().getString(R.string.from_camera)
          );
        }
      } else {
        imagesModel.addNewImageInAdapter(
            ImagePicker.getFirstImageOrNull(resultData).getPath(),
            galleryOfImagesActivity.getResources().getString(R.string.from_gallery)
        );
      }
    }
  }
}
