package com.talalbintahir.galleryimagesupload.activity;

import android.app.Activity;
import android.net.Uri;

public interface ImageCropAndRotatePresenter {

  void editImage(int selectedPosition);

  void saveEditedImage(String imagePath);

  void getImagePathFromURL();

  void editImageFromURL(Uri savedImageURI, Activity activity);
}
