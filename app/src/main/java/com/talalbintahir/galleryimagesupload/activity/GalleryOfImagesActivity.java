package com.talalbintahir.galleryimagesupload.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.esafirm.imagepicker.features.ImagePicker;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.adapter.ImageAdapter.ClickListener;
import com.talalbintahir.galleryimagesupload.adapter.ImageAdapter.RecyclerTouchListener;
import com.talalbintahir.galleryimagesupload.fragment.ImageDetailFragment;

public class GalleryOfImagesActivity extends AppCompatActivity implements GalleryOfImagesView {


  private static final String[] permissions = new String[] {
      Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE };

  // Request code for external and internal storage permissions
  private static final int REQUEST_CODE_PERMISSIONS = 1001;

  // elements of this activity
  private ProgressDialog pDialog;
  private AlertDialog unlockDialog;
  private RecyclerView recyclerView;
  private GalleryOfImagesPresenter galleryOfImagesPresenter;
  private FloatingActionButton addNewImageButton;

  // a simple bool to check first launch
  private boolean checkFirstLaunch = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTheme(R.style.AppTheme);
    setContentView(R.layout.gallery_of_images);


    recyclerView = findViewById(R.id.rv_images);
    pDialog = new ProgressDialog(this);
    addNewImageButton = findViewById(R.id.btn_add_new_image);
    addNewImageButton.setImageResource(R.drawable.add_new_image_btn_logo);

    final TextView instructionTextView = findViewById(R.id.tv_instruction);
    instructionTextAnimation(instructionTextView);

    // calling the presenter to fetch online images and then add set them on recycleView
    galleryOfImagesPresenter = new GalleryOfImagesPresenterImpl(this, getApplicationContext());
    galleryOfImagesPresenter.getOnlineImages();


    // method to invoke onClickListener
    setOnClickListeners();

  }

  /**
   * This method sets the onClickListeners for recyclerView and addNewImageButton
   */
  private void setOnClickListeners() {

    recyclerView.addOnItemTouchListener(
        new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
          @Override
          public void onClick(View view, int position) {
            // the presenter call method with position of the image to open image detail fragment
            galleryOfImagesPresenter.openImageDetailFragment(position);
          }
          @Override
          public void onLongClick(View view, int position) {}
        }));

    addNewImageButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        // the presenter call method to open ImagePicker to select image from either gallery or camera
        galleryOfImagesPresenter.openImagePicker();
      }
    });
  }

  /**
   * This method animates the instruction text on the Gallery of Image view
   * The text fades away after 2 seconds
   * @param tv is the TextView to be animated
   */
  private void instructionTextAnimation(final TextView tv) {
    tv.animate()
        .alpha(0.0f)
        .setDuration(2000)
        .setStartDelay(1000)
        .setListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            tv.setVisibility(View.GONE);
          }
        });
  }

  /**
   * This method sets the recyclerView adapter by the given adapter
   * @param adapter is the adapter that needs to be set
   */
  @Override
  public void setRecycleViewAdapter(RecyclerView.Adapter adapter) {
    LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setAdapter(adapter);
  }

  /**
   * This method start the image detail fragment on the given position
   * @param position is the position of image
   */
  @Override
  public void startImageDetailFragment(int position) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ImageDetailFragment newFragment = new ImageDetailFragment();
    Bundle bundle = new Bundle();
    if (checkFirstLaunch) {
      // Checks for the first launch to hide the instructions text
      bundle.putBoolean(getString(R.string.check), true);
      checkFirstLaunch = false;
    } else {
      bundle.putBoolean(getString(R.string.check), false);
    }
    bundle.putInt(getResources().getString(R.string.selected_position), position);
    newFragment.setArguments(bundle);
    newFragment.show(ft, getString(R.string.slideshow));
  }

  /**
   * This method add a new image from gallery or camera into the app
   */
  @Override
  public void addNewImage() {
    // Image picker let the user pick image from gallery or take new picture from camera
    ImagePicker.create(GalleryOfImagesActivity.this)
            .folderMode(true) // folder mode
            .toolbarFolderTitle(getString(R.string.folder_text)) // folder title
            .toolbarArrowColor(Color.WHITE) // Toolbar arrow color
            .showCamera(true)
            .imageDirectory(getString(R.string.camera_text)) // directory name for captured image
            .enableLog(false)// disabling log
            .start(); // start image picker activity with request code
  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
    // the presenter forward these prams to its method to add new image
    galleryOfImagesPresenter.addNewImage(requestCode, resultCode, resultData);
  }

  /**
   * This method show the progress dialog when the online images are getting fetched
   */
  @Override
  public void showProgressDialog() {
    pDialog.setMessage(getResources().getString(R.string.downloading_pictures_text));
    pDialog.show();
  }

  /**
   * This method hides the progress dialog when the online images are fetched
   */
  @Override
  public void hideProgressDialog() {
    pDialog.dismiss();
  }

  /**
   * This method checks the permission of storage.
   * @return true, if the user has given access
   *        false, if the user has not given access.
   */
  @Override
  public boolean checkPermission() {
    for (String permission : permissions) {
      int result = ActivityCompat.checkSelfPermission(getApplicationContext(), permission);

      if(result == PackageManager.PERMISSION_DENIED) return false;
    }

    return true;
  }

  /**
   * This method show the permission dialog
   */
  @Override
  public void showPermissionDialog() {
    ActivityCompat.requestPermissions(this,permissions, REQUEST_CODE_PERMISSIONS);
  }

  /**
   * This method show the alert dialog which tells the user that the user has denied permission and
   * now need to give access manually
   */
  @Override
  public void showUnlockPermissionsDialog() {
    if(unlockDialog == null) {
      unlockDialog = new AlertDialog.Builder(GalleryOfImagesActivity.this)
          .setTitle(getString(R.string.without_permissions_text))
          .setMessage(getString(R.string.without_permissions_text_description))
          .create();
    }
    unlockDialog.show();
  }

  /**
   * This method checks to show the permission dialog or not
   * @return true, if the user don't give access
   *         false, if the user give access
   */
  @Override
  public boolean shouldShowDialog() {
    for(String permission : permissions) {
      boolean b = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
      if(!b) return false;
    }
    return true;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if(requestCode == REQUEST_CODE_PERMISSIONS ) {
      if(!allPermissionsGranted(grantResults)) {
        galleryOfImagesPresenter.permissionDenied();
      }
    }
  }

  /**
   * This method checks all the permission granted or not
   * @param grantResults is the grantResult of user
   * @return true, if granted
   *         false, if denied
   */
  private boolean allPermissionsGranted(int[] grantResults) {
    for (int result : grantResults) {
      if(result == PackageManager.PERMISSION_DENIED)
        return false;
    }

    galleryOfImagesPresenter.openImagePicker();
    return true;
  }

  @Override
  public void onBackPressed() {
    finish();
  }

}


