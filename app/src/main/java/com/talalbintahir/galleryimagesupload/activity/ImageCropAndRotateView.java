package com.talalbintahir.galleryimagesupload.activity;

import android.app.Activity;
import android.net.Uri;

public interface ImageCropAndRotateView {

  void openCropAndRotateActivity(Uri uri, Activity activity);

  void callMainActivity();
}
