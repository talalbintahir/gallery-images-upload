package com.talalbintahir.galleryimagesupload.activity;

import android.app.Activity;
import android.net.Uri;
import com.talalbintahir.galleryimagesupload.helper.SaveImageFromURL;
import com.talalbintahir.galleryimagesupload.model.ImagesModel;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;

/**
 * Class to implement method from presenter class of Image Crop and Rotate activity which connects its View and Model.
 */
public class ImageCropAndRotatePresenterImpl implements ImageCropAndRotatePresenter {

 private static ImageCropAndRotateActivity imageCropAndRotateActivity;
  private final ImagesModel imagesModel = new ImagesModelImpl(); // model instance
  private int selectedPosition = 0; // position of the selected image to be edited
  ImageCropAndRotatePresenterImpl(ImageCropAndRotateActivity imageCropAndRotateActivity) {
   this.imageCropAndRotateActivity = imageCropAndRotateActivity;
  }
  public ImageCropAndRotatePresenterImpl() {
  }

 /**
  * This method open the crop and rotate activity to edit image on the given position
  * @param selectedPosition is the position of the image
  */
  @Override
  public void editImage(int selectedPosition) {
    this.selectedPosition = selectedPosition;
   imageCropAndRotateActivity.openCropAndRotateActivity(
           imagesModel.getUriOfSelectedImage(selectedPosition), // instance of the model gets the URI of image on the basis of selectedPosition
           imageCropAndRotateActivity);
  }

 /**
  * This method saves the edited image on the adapter
  * @param imagePath is the path of the edited image
  */
  @Override
  public void saveEditedImage(String imagePath) {
   imagesModel.addEditedImageInAdapter(imagePath, selectedPosition, imageCropAndRotateActivity);
   imageCropAndRotateActivity.callMainActivity();
  }

 /**
  * This is method calls the helper class to download and save online image
  */
 @Override
  public void getImagePathFromURL() {
    new SaveImageFromURL(imageCropAndRotateActivity).execute(imagesModel.getUriOfSelectedImage(selectedPosition).toString());

  }

 /**
  * This method edit the image from uri and activity
  * @param savedImageURI of the online image
  * @param activity where the image exist
  */
  @Override
  public void editImageFromURL(Uri savedImageURI, Activity activity) {
   imageCropAndRotateActivity.openCropAndRotateActivity(savedImageURI, activity);

  }
}
