package com.talalbintahir.galleryimagesupload.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.talalbintahir.galleryimagesupload.R;
import com.theartofdev.edmodo.cropper.CropImage;
import java.io.IOException;

/**
 * This is an image crop and rotate activity that receives and intent bundle and call instance of CropImage library
 */
public class ImageCropAndRotateActivity extends AppCompatActivity implements
    ImageCropAndRotateView {

  private static final int ON_BACK_PRESSED = 0;
  private ImageCropAndRotatePresenter imageCropAndRotatePresenter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    int selectedPosition = getIntent().getIntExtra(getString(R.string.selected_position), 0);
    imageCropAndRotatePresenter = new ImageCropAndRotatePresenterImpl(this); // the presenter instance of this activity
    imageCropAndRotatePresenter.editImage(selectedPosition); // presenter calls the method to editImage

  }

  /**
   * This method returns the result of edited image
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == ON_BACK_PRESSED) {
      // checks if the back button was pressed without editing the image
      callMainActivity();
    } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
      // checks if the result need to be handled by cropImage
      CropImage.ActivityResult result = CropImage.getActivityResult(data);
      if (resultCode == RESULT_OK) {
        //checks if the image has been edited or not
        String savedImageURL = null;
        Uri imageUri = result.getUri(); // get the uri of the edited image
        try {
          // try to save it as a bitmap on storage
          Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
          savedImageURL = MediaStore.Images.Media.insertImage(
              getContentResolver(),
              bitmap,
              getString(R.string.cropped_image_title),
              getString(R.string.cropped_image_description)
          );
        } catch (IOException e) {
          e.printStackTrace();
        }
        if (savedImageURL != null) {
          // if the image was successfully stored, it will be saved on the adapter
          imageCropAndRotatePresenter.saveEditedImage(savedImageURL);
        }
      } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
        // checks if the image was online or not
        imageCropAndRotatePresenter.getImagePathFromURL(); // call the helper method to download and save online image
      }
    }
  }

  /**
   * This method call instance of CropImage library by passing activity and uri of the image
   * @param uri of the image
   * @param activity from where the image exist
   */
  @Override
  public void openCropAndRotateActivity(Uri uri, Activity activity) {
    // CropImage library let the user edit the image by cropping or rotating the image
    CropImage
        .activity(uri)
        .setCropMenuCropButtonTitle(getResources().getString(R.string.done))
        .start(activity);
  }

  /**
   * This method call the Main activity which is Gallery of Image
   */
  @Override
  public void callMainActivity() {
    Intent intent = new Intent(this, GalleryOfImagesActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
  }

}
