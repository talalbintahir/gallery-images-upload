package com.talalbintahir.galleryimagesupload.activity;

import android.support.v7.widget.RecyclerView;

interface GalleryOfImagesView {

    void showProgressDialog();

    void hideProgressDialog();

    boolean checkPermission();

    void showPermissionDialog();

    void showUnlockPermissionsDialog();

    boolean shouldShowDialog();

    void setRecycleViewAdapter(RecyclerView.Adapter adapter);

    void startImageDetailFragment(int imagePosition);

    void addNewImage();
}
