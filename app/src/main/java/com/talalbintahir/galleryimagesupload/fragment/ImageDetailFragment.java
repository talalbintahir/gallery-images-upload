package com.talalbintahir.galleryimagesupload.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.activity.ImageCropAndRotateActivity;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;
import com.talalbintahir.galleryimagesupload.adapter.ImageDetailViewPagerAdapter;

/**
 * This an Image Detail Fragment which show the details of the selected image on full screen.
 * Also give an option to user to edit the image
 */
public class ImageDetailFragment extends DialogFragment implements ImageDetailView {

  // elements of this fragment
  private ViewPager viewPagerImageDetail;
  private ImageDetailPresenter imageDetailPresenter;
  private TextView imageCount, imageTitle, imageDate;
  private int selectedPosition = 0;
  private FloatingActionButton edit_image;

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.image_detail_fragment_slider, container, false);
    viewPagerImageDetail = v.findViewById(R.id.viewpager_image_detail);
    imageCount = v.findViewById(R.id.tv_image_count);
    imageTitle = v.findViewById(R.id.tv_image_title);
    imageDate = v.findViewById(R.id.tv_image_date);

    edit_image = v.findViewById(R.id.btn_edit_image);
    edit_image.setImageResource(R.drawable.edit_image_btn_logo);
    if (getArguments() != null) {
      // checks if the given arguments are not null
      selectedPosition = getArguments().getInt(getString(R.string.selected_position));
    }
    boolean firstLaunchCheck = getArguments().getBoolean(getString(R.string.check));

    // Presenter of the fragment
    imageDetailPresenter = new ImageDetailPresenterImpl(this);
    imageDetailPresenter.getMyViewImageAdapter(); // the presenter gets the image detail adapter
    setCurrentItem(selectedPosition); // this method sets the selected position as per the image selected
    setOnClickListeners(); // this method calls onClickerListeners for the button

    if (firstLaunchCheck) {
      // checks the first launch to hide the animation of the instruction text
      final TextView instructionTextView = v.findViewById(R.id.tv_edit_instruction);
      instructionEditTextAnimation(instructionTextView);
    }
    return v;
  }

  /**
   * This method animates the instruction text on the Image Detail Fragment
   * The text fades away after 2 seconds
   * @param tv is the TextView to be animated
   */
  private void instructionEditTextAnimation(final TextView tv) {
    tv.setVisibility(View.VISIBLE);
    tv.animate()
        .alpha(0.0f)
        .setDuration(2000)
        .setStartDelay(1000)
        .setListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            tv.setVisibility(View.GONE);
          }
        });
  }

  /**
   * Sets the onClickerListeners for ViewPager and EditImageButton
   */
  private void setOnClickListeners() {

    // page change listener
    viewPagerImageDetail.addOnPageChangeListener(new OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        selectedPosition = position;
        imageDetailPresenter.setCurrentItem(position);
      }

      @Override
      public void onPageSelected(int position) {
      }

      @Override
      public void onPageScrollStateChanged(int state) {
      }
    });

    // edit button listener
    edit_image.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        imageDetailPresenter.editImage(view, selectedPosition);
      }
    });
  }

  /**
   * This method sets the current item as per image position
   * @param position is the position of the image
   */
  private void setCurrentItem(int position) {
    viewPagerImageDetail.setCurrentItem(position, false); // sets the current item of Viewpager
    imageDetailPresenter.setCurrentItem(selectedPosition); // sets the current in presenter
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
  }

  @Override
  public void setAdapter(ImageDetailViewPagerAdapter adapter) {
    viewPagerImageDetail.setAdapter(adapter);
  }

  /**
   * This method display meta info of the image
   * @param imageSelected, selected image
   * @param imageCountText, count text of the image
   */
  @Override
  public void displayMetaInfo(ImagesModelImpl imageSelected, String imageCountText) {
    imageCount.setText(imageCountText);
    imageTitle.setText(imageSelected.getImageTitle());
    imageDate.setText(imageSelected.getImageCreatedAt());
  }

  /**
   * This method starts the edit image activity of the given image
   * @param view to get the context
   * @param bundle to be forwarded to edit image activity
   */
  @Override
  public void startEditImageActivity(View view, Bundle bundle) {
    Intent intent = new Intent(view.getContext(), ImageCropAndRotateActivity.class);
    intent.putExtras(bundle);
    startActivity(intent);
  }
}