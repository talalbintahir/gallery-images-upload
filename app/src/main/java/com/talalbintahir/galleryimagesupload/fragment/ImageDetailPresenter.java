package com.talalbintahir.galleryimagesupload.fragment;

import android.view.View;

public interface ImageDetailPresenter {

  void getMyViewImageAdapter();

  void setCurrentItem(int selectedPosition);

  void editImage(View view, int selectedPosition);
}
