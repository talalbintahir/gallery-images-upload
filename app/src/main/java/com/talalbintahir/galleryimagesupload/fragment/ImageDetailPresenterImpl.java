package com.talalbintahir.galleryimagesupload.fragment;

import android.os.Bundle;
import android.view.View;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.adapter.ImageDetailViewPagerAdapter;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;
import java.util.ArrayList;

/**
 * Class to implement method from presenter class of Image Detail activity which connects its View and Model.
 */
public class ImageDetailPresenterImpl implements ImageDetailPresenter {

  private final ImageDetailFragment imageDetailFragment; // Instance of Image Detail Fragment
  private final ImagesModelImpl imagesModelImpl = new ImagesModelImpl(); // Instance of Image Model
  private ArrayList<ImagesModelImpl> images;

  ImageDetailPresenterImpl(ImageDetailFragment imageDetailFragment) {
    this.imageDetailFragment = imageDetailFragment;
  }

  /**
   * This method get the ViewImageAdapter and set it to viewPagerImageDetail adapter
   */
  @Override
  public void getMyViewImageAdapter() {
    images = imagesModelImpl.getImages();
    ImageDetailViewPagerAdapter myViewPagerAdapter = new ImageDetailViewPagerAdapter(
        imageDetailFragment.getActivity(), images);
    imageDetailFragment.setAdapter(myViewPagerAdapter);

  }

  /**
   * This method set the current item to display its meta on Image Detail Fragment
   * @param selectedPosition is the position of selected image
   */
  @Override
  public void setCurrentItem(int selectedPosition) {
    String imageCountText = (selectedPosition + 1) + " of " + images.size();
    imageDetailFragment.displayMetaInfo(images.get(selectedPosition), imageCountText);
  }

  /**
   * This method start edit image activity with the selectedPosition
   * @param view to get the context
   * @param selectedPosition is the position of image to be added on the bundle
   */
  @Override
  public void editImage(View view, int selectedPosition) {
    Bundle bundle = new Bundle();
    bundle.putSerializable(imageDetailFragment.getResources().getString(R.string.images), images);
    bundle.putInt(imageDetailFragment.getResources().getString(R.string.selected_position), selectedPosition);
    imageDetailFragment.startEditImageActivity(view, bundle);
  }
}
