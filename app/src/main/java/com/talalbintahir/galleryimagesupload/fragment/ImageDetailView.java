package com.talalbintahir.galleryimagesupload.fragment;

import android.os.Bundle;
import android.view.View;
import com.talalbintahir.galleryimagesupload.model.ImagesModelImpl;
import com.talalbintahir.galleryimagesupload.adapter.ImageDetailViewPagerAdapter;

public interface ImageDetailView {

  void setAdapter(ImageDetailViewPagerAdapter adapter);

  void displayMetaInfo(ImagesModelImpl imageSelected, String imageCountText);

  void startEditImageActivity(View view, Bundle bundle);
}
