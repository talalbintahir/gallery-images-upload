package com.talalbintahir.galleryimagesupload.model;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;

import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesPresenter;
import java.util.ArrayList;

public interface ImagesModel {

  RecyclerView.Adapter getAdapter();

  ArrayList<ImagesModelImpl> getImages();

  void fetchImages(GalleryOfImagesPresenter galleryOfImagesPresenter, Context context);

  void addNewImageInAdapter(String path, String typeText);

  Uri getUriOfSelectedImage(int selectedPosition);

  void addEditedImageInAdapter(String imagePath, int selectedPosition, Context context);
}
