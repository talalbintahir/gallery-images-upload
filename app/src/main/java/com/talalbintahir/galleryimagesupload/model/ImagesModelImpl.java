package com.talalbintahir.galleryimagesupload.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.talalbintahir.galleryimagesupload.R;
import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesPresenter;
import com.talalbintahir.galleryimagesupload.adapter.ImageAdapter;
import com.talalbintahir.galleryimagesupload.helper.VolleyNetwork;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is a class that implements imagesModel and serializable
 */
public class ImagesModelImpl implements ImagesModel, Serializable {

  public static ArrayList<ImagesModelImpl> images;
  private RecyclerView.Adapter adapter;
  private Context context;

  // serializable elements
  private String imageTitle;
  private  String imagePath;
  private String imageCreatedAt;

  // getter and setter methods for Image title
  public String getImageTitle() {
    return imageTitle;
  }

  private void setImageTitle(String name) {
    this.imageTitle = name;
  }
  // getter and setter methods for Image path
  public String getImagePath() {
    return imagePath;
  }

  private void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  // getter and setter methods for Image created at
  public String getImageCreatedAt() {
    return imageCreatedAt;
  }

  private void setImageCreatedAt(String imageCreatedAt) {
    this.imageCreatedAt = imageCreatedAt;
  }


  /**
   * This method returns the adapter of the images
   * @return the adapter
   */
  @Override
  public Adapter getAdapter() {
    adapter = new ImageAdapter(context, images);
    return adapter;
  }

  /**
   * This method returns the ArrayList of the images
   * @return images
   */
  @Override
  public  ArrayList<ImagesModelImpl> getImages() {
    return images;
  }

  /**
   * This method fetch online images from given endpoint
   * @param galleryOfImagesPresenter to call ProgressDialog
   * @param context to  be used by sharedPreferences
   */
  @Override
  public void fetchImages(final GalleryOfImagesPresenter galleryOfImagesPresenter, final Context context) {
    this.context = context;

    if (getImageListOnSharedPreferences(context.getResources().getString(R.string.images)) != null) {
      // checks if data exists in sharedPreferences
      images = getImageListOnSharedPreferences(context.getResources().getString(R.string.images));
    }
    else {
      images = new ArrayList<>();
      galleryOfImagesPresenter.showProgressDialog();
      JsonArrayRequest req = new JsonArrayRequest(context.getResources().getString(R.string.endpoint), // a JSON request to be added in Request Queue by Volley
          new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
              galleryOfImagesPresenter.hideProgressDialog();
              for (int i = 0; i < response.length(); i++) {
                try {
                  // add the JSON response in the images array list
                  JSONObject object = response.getJSONObject(i);
                  ImagesModelImpl image = new ImagesModelImpl();
                  image.setImageTitle(object.getString(context.getResources().getString(R.string.online_image_name)));

                  JSONObject imageBundle = object.getJSONObject(context.getResources().getString(R.string.online_image_bundle));
                  image.setImagePath(imageBundle.getString(context.getResources().getString(R.string.online_image_path)));
                  image.setImageCreatedAt(object.getString(context.getResources().getString(R.string.online_image_created_at)));
                  images.add(image);
                } catch (JSONException e) {
                 // Log.e(context.getPackageName(), "Json parsing error: " + e.getMessage());
                }
              }
              adapter.notifyDataSetChanged(); // notify adapter that data has been changed
            }
          }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          galleryOfImagesPresenter.hideProgressDialog();
        }
      });
      VolleyNetwork.getInstance().addToRequestQueue(req); // Add the JSONRequest in Volley's request queue
    }
  }

  /**
   * This method add image of the given path and its type on images adapter
   * @param path of the given image
   * @param typeText type of the image
   */
  @Override
  public void addNewImageInAdapter(String path, String typeText) {
          ImagesModelImpl newImage = new ImagesModelImpl();
          newImage.setImageTitle(typeText);
          newImage.setImagePath(context.getResources().getString(R.string.content_provider) + path);
          newImage.setImageCreatedAt(getImageCreatedAt(path));
          images.add(newImage);
          adapter.notifyDataSetChanged();
          setImageListOnSharedPreferences(images, context); // update the sharedPreferences
  }

  /**
   * Get the Uri of the selected image
   * @param selectedPosition of the selected image
   * @return uri of the selected image
   */
  @Override
  public Uri getUriOfSelectedImage(int selectedPosition) {
   return Uri.parse(images.get(selectedPosition).getImagePath());
  }

  /**
   * This method adds an edited image on the images adapter
   * @param imagePath path of the selected image
   * @param selectedPosition position of selected image
   * @param context context of the activity
   */
  @Override
  public void addEditedImageInAdapter(String imagePath, int selectedPosition, Context context) {
          ImagesModelImpl image = new ImagesModelImpl();
          image.setImageTitle(context.getResources().getString(R.string.cropped_image_title));
          image.setImagePath(imagePath);
          image.setImageCreatedAt(getCurrentDate());
          images.set(selectedPosition,image);
          setImageListOnSharedPreferences(images, context);
  }

  /**
   * Add the image list on sharedPreferences
   * @param images ArrayList of Images
   * @param context context of the activity
   */
  private void setImageListOnSharedPreferences(ArrayList<ImagesModelImpl> images, Context context) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    SharedPreferences.Editor editor = prefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(images);
    editor.putString(context.getResources().getString(R.string.images), json);
    editor.apply();
  }

  /**
   * Returns the arrayList from sharedPreferences
   * @param key string for sharedPreferences
   * @return arrayList
   */
  private ArrayList<ImagesModelImpl> getImageListOnSharedPreferences(String key) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    Gson gson = new Gson();
    String json = prefs.getString(key, null);
    Type type = new TypeToken<ArrayList<ImagesModelImpl>>() {
    }.getType();
    return gson.fromJson(json, type);
  }

  /**
   * Returns date as string of the given image when it was last modified
   * @param imagePath path of the given image
   * @return date as string
   */
  private String getImageCreatedAt(String imagePath) {
    File file = new File(imagePath);
    DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
    return dateFormat.format(file.lastModified());
  }

  /**
   * Returns the current date
   * @return date as string
   */
  private String getCurrentDate() {
    DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
    return  dateFormat.format(new Date());
  }

}
