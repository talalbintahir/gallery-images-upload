package com.talalbintahir.galleryimagesupload;


import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesActivity;
import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesPresenter;
import com.talalbintahir.galleryimagesupload.activity.GalleryOfImagesPresenterImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;


public class UnitTest {

    @Mock
    private
    GalleryOfImagesActivity activity;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * This test opens up the imagePicker checks for the external and internal storage
     * as well as camera permissions.
     */
    @Test
    public void openImagePicker_checksPermissions() {
        GalleryOfImagesPresenter presenter = new GalleryOfImagesPresenterImpl(activity, activity.getApplicationContext());
        presenter.openImagePicker();
        verify(activity).checkPermission();
    }

    /**
     * This test opens up the imagePicker and show the permission dialog.
     * As the user has not permit the access.
     */
    @Test
    public void openImagePicker_showPermissionDialog() {
        when(activity.checkPermission()).thenReturn(false);
        GalleryOfImagesPresenter presenter = new GalleryOfImagesPresenterImpl(activity, activity.getApplicationContext());
        presenter.openImagePicker();
        verify(activity).showPermissionDialog();
    }

    /**
     * This test show the dialog when the user has denied the permission.
     */
    @Test
    public void permissionDenied_showNeedPermissionsDialog_whenShouldRationale() {
        when(activity.shouldShowDialog()).thenReturn(true);
        GalleryOfImagesPresenter presenter = new GalleryOfImagesPresenterImpl(activity, activity.getApplicationContext());
        presenter.permissionDenied();
        verify(activity).showPermissionDialog();
    }

    /**
     * This test doesn't show the dialog when the user has denied the permission to never seeing it again.
     */
    @Test
    public void permissionDenied_doNotShowNeedPermissionsDialog_whenNotShouldRationale() {
        when(activity.shouldShowDialog()).thenReturn(false);
        GalleryOfImagesPresenter presenter = new GalleryOfImagesPresenterImpl(activity, activity.getApplicationContext());
        presenter.permissionDenied();
        verify(activity, never()).showPermissionDialog();
    }

    /**
     * This test show the alert dialog telling the user that they have denied the permission and
     * now they have to permit everything manually.
     */
    @Test
    public void permissionDenied_showUnlockPermissionsDialog_whenNotShouldRationale() {
        when(activity.shouldShowDialog()).thenReturn(false);
        GalleryOfImagesPresenter presenter = new GalleryOfImagesPresenterImpl(activity, activity.getApplicationContext());
        presenter.permissionDenied();
        verify(activity).showUnlockPermissionsDialog();
    }
}