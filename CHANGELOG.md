## Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

---
## [1.0.4](https://gitlab.com/talalbintahir/gallery-images-upload/tags/1.0.4) (Released: 1.08.2018)

* [#6](https://gitlab.com/talalbintahir/gallery-images-upload/issues/6): Solved existing bugs and add comments for readability.
* [#5](https://gitlab.com/talalbintahir/gallery-images-upload/issues/5): Added Unit and UI tests.


## [1.0.3](https://gitlab.com/talalbintahir/gallery-images-upload/tags/1.0.3) (Released: 30.07.2018)

* [#4](https://gitlab.com/talalbintahir/gallery-images-upload/issues/4): Improved the quality of the code and app performance.

## [1.0.2](https://gitlab.com/talalbintahir/gallery-images-upload/tags/1.0.2) (Released: 24.07.2018)

* [#3](https://gitlab.com/talalbintahir/gallery-images-upload/issues/3): Added an image edit functionality.

## [1.0.1](https://gitlab.com/talalbintahir/gallery-images-upload/tags/1.0.1) (Released: 23.07.2018)

* [#2](https://gitlab.com/talalbintahir/gallery-images-upload/issues/2): Added an image picker button with functionality.

## [1.0.0](https://gitlab.com/talalbintahir/gallery-images-upload/tags/1.0.0) (Released: 22.07.2018)

* [#1](https://gitlab.com/talalbintahir/gallery-images-upload/issues/1): Added a gallery view.




