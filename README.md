# Gallery image upload
<p float="center">
  
  <img src="https://gitlab.com/talalbintahir/gallery-images-upload/uploads/1ac3fc0c6462ec722f7286558d74a709/splash_screen_logo.png" width="300" />

</p>


This is a gallery image application in which the user can see previously uploaded images and can add new image either via camera or gallery.
It also has feature to rotate and crop the image


# Implementation Notes

I have tried my best to follow MVP design pattern and implement as per their guidlines. You will find the model, view and presenter layers in this project. 

The libraries I used in this project are as follows:

- [Glide](https://github.com/bumptech/glide): to load the images from Gallery and make it look attractive

- [ImagePicker](https://github.com/esafirm/android-image-picker): to pick image from Gallery or take a new picture from camera

- [Android-Image-Cropper](https://github.com/ArthurHub/Android-Image-Cropper): to crop and rotate the image

Initialy, I'm using a public JSONAPI added in the project just to fetch some online images. Other than that I have not implemented any server side part, as the requirement of this project
was more focused on Frontend part rather than the backend. 

Below you will find some screenshots to have an idea of the app and its functionlity. 

## Sreenshots
Gallery of Images            |  Image Picker
:-------------------------:|:-------------------------:
![WhatsApp_Image_2018-08-01_at_2.33.41_AM](/uploads/c2538df71755dbcd78f06fdb5b8a174c/WhatsApp_Image_2018-08-01_at_2.33.41_AM.jpeg)  |  ![WhatsApp_Image_2018-08-01_at_2.33.41_AM__1_](/uploads/fa5bb1bcc0ac61b934e8240750e324ac/WhatsApp_Image_2018-08-01_at_2.33.41_AM__1_.jpeg)



Image Detail Screen            |  Edit image (Crop/Rotate)
:-------------------------:|:-------------------------:
![WhatsApp_Image_2018-08-01_at_2.33.42_AM](/uploads/116053ab92503179ed6fe884dee70cbb/WhatsApp_Image_2018-08-01_at_2.33.42_AM.jpeg)  | ![WhatsApp_Image_2018-08-01_at_2.33.42_AM__1_](/uploads/8c10efbdf80d820e2ca3821c08ed0bdf/WhatsApp_Image_2018-08-01_at_2.33.42_AM__1_.jpeg)



##  Installation instructions

1. Open Android Studio
2. Click "checkout project from version control" and select GitHub
3. Copy the following into "VCS Repository URL" : "https://gitlab.com/talalbintahir/gallery-images-upload.git"
4. Choose a path as you like to clone the project
6. Click "clone"

DONE! 😄 

## Bugs

If you encounter any bugs. Please [report them](https://gitlab.com/talalbintahir/gallery-images-upload/issues).